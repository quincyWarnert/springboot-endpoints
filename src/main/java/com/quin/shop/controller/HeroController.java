package com.quin.shop.controller;


import com.quin.shop.model.Hero;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class HeroController {

    @GetMapping("/heroes")
    public List<Hero> getAllHeroes() {
        return Arrays.asList(
                new Hero(23, "Lebron", "James", "LBJ@mail.net"),
                new Hero(2, "Kawhi", "Leonard", "KLW@mail.net"),
                new Hero(35, "Kevin", "Durant", "KD@mail.net")
        );
    }

}
