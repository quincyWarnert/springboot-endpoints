package com.quin.shop.repository;

import com.quin.shop.model.Players;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayersRepository extends JpaRepository<Players, Long> {

    @Query("select u from Players u where u.firstName=?1")
    Players findByFirstName(String firstName);

}
